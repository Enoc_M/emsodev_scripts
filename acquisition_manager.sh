#!/bin/bash
#===================================================================#
#                  Acquisition Manager script                       #
#===================================================================#
# This script provides an easy way to control the acquisition       #
# system status for the EMSODEV project. It starts, stops or checks #
# the status of all the software components involved in the         #
# acquisition process.                                              #
#                                                                   #
# argument 1 : command (defined in the "commands" array)            #
# argument 2 : instrument (defined in the "software_components"     #
#              array                                                #
#===================================================================#
# @author : Enoc Martínez                                           #
# @contact : enoc.martinez@upc.edu                                  #
# @organization : Universitat Politécnica de Catalunya	(UPC)       #
#                                                                   #
# This script has been developed as a part of the EMSODEV project   #
#===================================================================#


#-------------------- Global variables -----------------------#
execution_path="/home/sarti/testbed" 
#driver=SWE_Bridge_v0.32
driver=swe_bridge_emsodev
PIDs_file=.PIDs
tmp_files_path="/home/sarti/testbed/temp"
SMLs_path="/home/sarti/testbed/EXI-SensorMLs"
tmp_files_path="/home/sarti/testbed/temp"

#---------- Acquisition manager implemented commands ---------#
commands=("start" "stop" "restart" "status")
#--------------------- Software components -------------------#
# components that are controlled, they 
# can be individual processes (i.e. "sos_proxy") or driver
# oriented to a specific instrument (i.e. SBE37)

software_components=("egim_bin_to_ascii" "sos_proxy" "auto_connect")
instruments=("SBE37" "EGIM_status" "ECO_NTU" "Aanderaa_4381" "SBE54" "ADCP_workhorse")
# it is also possible start all the instruments and the acquisition software components 
# using "all"




#temporal files generated by the different drivers. Used to show the last values
tmp_files=("SBE37_data.tmp" "EGIM_status_result.tmp" "ecoNTU_result.tmp" "Aanderaa_result.tmp" "SBE54_result.tmp" "ADCP_result.tmp")

#---------- Colour codes --------------#
red=$'\e[1;31m'
grn=$'\e[1;32m'
yel=$'\e[1;33m'
blu=$'\e[1;34m'
mag=$'\e[1;35m'
cyn=$'\e[1;36m'
end=$'\e[0m'

#---------------------------------------------------------------------------#
#--------------------------- FUNCTION DEFINITIONS --------------------------#
#---------------------------------------------------------------------------#


#-------------------------------------------------------------------#
#---------------------------- Is Alive -----------------------------#
#-------------------------------------------------------------------#
# This function takes a PID as argument and checks if the process	#
# is still running													#
# 																	#
# argument1: instrument												#
# return values: 1 is alive, 0 not alive							#
#-------------------------------------------------------------------#
show_usage() {
	echo "usage: $SMLs_path0 <command> <instrument/software_component>"
	
	printf "\nCommands: "
	for i in "${commands[@]}"; do
		printf "\"$i\"  "
	done
	printf "\n"
	printf "instruments: "
	for i in "${instruments[@]}"; do
		printf "\"$i\"  "
	done
	printf "\n"
	printf "software_components: "
	for i in "${software_components[@]}"; do
		printf "\"$i\"  "
	done
	printf "\n"
}

#-------------------------------------------------------------------#
#---------------------------- Is Alive -----------------------------#
#-------------------------------------------------------------------#
# This function takes a PID as argument and checks if the process	#
# is still running													#
# 																	#
# argument1: instrument												#
# return values: 1 is alive, 0 not alive							#
#-------------------------------------------------------------------#
is_alive(){
	arg="$1"
	if [ $arg == "egim_bin_to_ascii" ]; then
		arg="java"
	fi
	res=`ps -A | sed -n /${arg}/p`
	if [ "${res:-null}" = null ]; then
		return 0
	else
		return 1
	fi
}


#-------------------------------------------------------------------#
#-------------------------- Check status----------------------------#
#-------------------------------------------------------------------#
# This function takes a software component name (i.e. SBE37) and 	#
# checks if it is running or not and compares its status with the	#
# PIDs file. If there has been some changes (i.e. a specific 		#
# software component has ended) the status of the PIDs file is 		#
# updated.															#
# 																	#
# argument1: software component										#
# return values: 1 is alive, 0 not alive							#
#-------------------------------------------------------------------#
check_status(){
	#argument1: Instrument
	#return value	1 - process is running
	#				0 - proccess is stopped

	PID=$(cat $PIDs_file | grep $1 | awk '{print $3}') # the the PID from the PIDs file
	status=$(cat $PIDs_file | grep $1 | awk '{print $2}') # get the status from the PIDs file
	
	is_alive $PID #check if the process is still alive
	alive=$?
	
	if [[ $alive == 1  &&  $status == "running" ]];	then
		# process is running
		return 1
	elif [[ $alive == 0  &&  $status == "running" ]]; then
		# process status does not match with the PIDs file
		echo "${red}Process $1 should be running, but its not!${end}"
		# update the PIDs file
		switch_status $1 stopped 0
		return 0

	else # the process is stopped
		return 0
	fi

}
#-------------------------------------------------------------------#
#----------------------- Check status info  ------------------------#
#-------------------------------------------------------------------#
# Checks the status of a project and shows a message according to 	#
# its status														#
#																	#
# argument 1 : software component name								#
#-------------------------------------------------------------------#
check_status_info(){
#arg1 instrument
	check_status $1
	ret=$?
	if [ $ret == 1 ]; then
		echo "${grn}Process $1 is running${end}"
	else
		echo "${blu}Process $1 is stopped${end}"
	fi
}


#-------------------------------------------------------------------#
#------------------------- Switch status  --------------------------#
#-------------------------------------------------------------------#
# Updates the information on the PIDs file.							#
#																	#
# argument 1 : software component name								#
# argument 2 : new status (i.e. "running" or "stopped"				#
# argument 3 : new PID, if the process is stopped it should be 0	#
#-------------------------------------------------------------------#
switch_status(){

	line_num=$(cat $PIDs_file | grep -n $1 | cut -f1 -d:) # get the line number
	#cat outputs the content of the file
	#grep -n adds the line number to match "2:instrument_name running 1234"
	#cut returns the line until the token ':'


	# updates the status information
	cat $PIDs_file | awk 'NR=='$line_num' {$2="'$2'"} { print }' > .tmp123 
	#awk searches for the argument 2 in line_num and prints the new argument 
	mv .tmp123 $PIDs_file # move the temp file to the PIDs_file

	# updates the PID information
	cat $PIDs_file | awk 'NR=='$line_num' {$3="'$3'"} { print }' > .tmp123
	mv .tmp123 $PIDs_file
}

#-------------------------------------------------------------------#
#------------------------- Is instrument  --------------------------#
#-------------------------------------------------------------------#
# Takes a name as instrument and checks if it is an instrument		#
#																	#
# argument 1 :  name												#
# return : 1 - instrument, 0 - not an instrument					#	
#-------------------------------------------------------------------#
is_instrument(){
	instr=0
	for i in "${instruments[@]}"; do
		if [ "$i" == "$1" ]; then
			instr=1
		fi
	done
	return $instr;
}

#-------------------------------------------------------------------#
#------------------------- Start Process  --------------------------#
#-------------------------------------------------------------------#
# Starts a software component and updates the information in the	#
# the PIDs file.													#
#																	#
# argument 1 : software component name								#
#-------------------------------------------------------------------#
start_process() {
	check_status $1 # check the status
	ret=$?
	if [ $ret == 1 ]; then # if it is already running do nothing
		echo "${yel}WARNING: Process for $1 already running!${end}"
		return -1
	fi
	
	is_instrument $1 # check if the argument is an instrument
	ret=$? #assign last value

	if [ $ret == 1 ];then 
		#exec the instrument
		# if it is a instrument start an instance of the SWE Bridge for the instrument
		# the configuration file should be "instrument_name.exi"
		echo "starting instrument $1"
			  nohup ./$driver -exi_file $SMLs_path/$1.exi > /dev/null 2>&1 & # start the process in background
		echo "nohup ./$driver -exi_file $SMLs_path/$1.exi > /dev/null 2>&1 &"
	else 
		#exec software components
		echo "soft component $1"
		if [ $1 == ${software_components[0]} ]; then
			#the first argument is a java process
			echo "starting java"
			nohup java -jar $1.jar > /dev/null 2>&1 &
		else
			#the rest of them are scripts
			echo "starting script"
			nohup ./$1.sh > /dev/null 2>&1 & # start the process in background
		fi
	
	fi	
	PID=$! # get the PID of the process
	echo "${grn}Process $1 started successfully${end}"
	switch_status $1 "running" $PID # update the PIDs file
}

#-------------------------------------------------------------------#
#--------------------------- Stop Process --------------------------#
#-------------------------------------------------------------------#
# This function takes an instrument name as argument1, reads the    #
# PIDs file, gets its PID and, if the process is still running, it	#
# is killed															#
# 																	#
# argument1: instrument												#
#-------------------------------------------------------------------#
stop_process(){
	check_status $1
	ret=$?
	if [ $ret == 0 ]; then
		echo "${yel}WARNING: Process $1 already stopped${end}"
		return 0
	fi

	PID=$(cat $PIDs_file | grep $1 | awk '{print $3}') #get the PID from the pids file
	echo "stopping process $1 with PID $PID"
	echo "killing process..."
	kill $PID
	switch_status $1 "stopped" 0
	echo "${blu}Process $1 stopped${end}"
	return 0

}
#-------------------------------------------------------------------#
#-------------------------- Geta array pos -------------------------#
#-------------------------------------------------------------------#
# This function takes an instrument name and returns the position	#
# of the instrument array where the instrument name is located		#
# 																	#
# argument1 : instrument											#
# return : 	array position											#
#-------------------------------------------------------------------#
get_array_pos(){
	for i in "${!instruments[@]}"; do
		if [[ "${instruments[$i]}" = "$1" ]]; then
			return ${i}
		fi
	done
}


#-------------------------------------------------------------------#
#-------------------------- Geta array pos -------------------------#
#-------------------------------------------------------------------#
# This function takes an instrument name and returns the position	#
# of the instrument array where the instrument name is located		#
# 																	#
# argument1 : instrument											#
# return : 	array position											#
#-------------------------------------------------------------------#
show_last_meas(){

	# check if the argument is a instrument
	flag=0
	for i in "${instruments[@]}"; do # Check if the command argument is correct
		if [ $i == $1 ]; then
			let flag=1
		fi
	done
	if [ $flag == 0 ]; then
		return 0
	fi

	get_array_pos $1
	ret=$?
#	echo "position is $ret"
	filename=$tmp_files_path/${tmp_files[$ret]}
	echo "looking file $filename"
	echo "showing last value in file $filename"
	echo "-----------------------------------------------------------"
	if [ ! -f $filename ]; then

		echo "${yel}WARNING: File $filename does not exist!${end}"
		echo "If the process is running the tempoaral file may be empty, try again a few seconds later"
		return 0
	fi
	tail -n 1 $filename | sed  's/@/\n/g' | sed  's/<sos:resultValues>/\n/g' > .sedtmpfile
	tail -n 2 .sedtmpfile | sed 's/#/    /g'
	echo "-----------------------------------------------------------"
	rm .sedtmpfile
	return 1
}

#-------------------------------------------------------------------#
#-------------------------- new PIDs file --------------------------#
#-------------------------------------------------------------------#
# This function generates a nwe PIDs file							#
# 																	#
#-------------------------------------------------------------------#
new_PIDs_file(){
	printf "======== Acquisition PIDs file ========\n" > $PIDs_file
	printf "Acquisition Status PID\n" >> $PIDs_file
	for i in "${instruments[@]}"; do
		printf "$i stopped 0\n" >> $PIDs_file
	done
	printf "---------------------------------------\n"
	for i in "${software_components[@]}"; do
		printf "$i stopped 0\n" >> $PIDs_file
	done
	printf "=======================================\n"  >> $PIDs_file
	cat $PIDs_file
	chmod 664 $PIDs_file
}

#---------------------------------------------------------------------------#
#------------------------------ PROGRAM START ------------------------------#
#---------------------------------------------------------------------------#
cd $execution_path #changing to execution path

if [ $# != 2 ]; then #check if the number of arguments matches
	show_usage
	exit 0
fi

flag=0
for i in "${commands[@]}"; do # Check if the command argument is correct
	if [ $i == $1 ]; then
		let flag=1
	fi
done

if [ $flag == 0 ]; then
	echo "wrong command \"$1\""
	show_usage
	exit 0
fi


let flag=0 # setting flag to 0
for i in "${software_components[@]}"; do # Check if the instrument argument is correct
	if [ $i == $2 ]; then
		let flag=1
	fi
done
for i in "${instruments[@]}"; do # Check if the instrument argument is correct
	if [ $i == $2 ]; then
		let flag=1
	fi
done
if [ $2 == "all" ]; then
	flag=1;
fi


if [ $flag == 0 ]; then
	echo "wrong instrument name \"$2\""
	show_usage
	exit 0
fi

#Check if the PIDs files exist
if [ ! -f $PIDs_file ]; then #if file does not exist
	echo "creating PIDs file..."
	new_PIDs_file #create a new file
fi

#---------------- START COMMAND ----------------#
if [ $1 == ${commands[0]} ]; then
	# start only 1 process
	if [ $2 != "all" ]; then
		start_process $2
else
	# start a process for each instrument
		for i in "${instruments[@]}"; do
				start_process $i
		done
		for i in "${software_components[@]}"; do
				start_process $i
		done 

	fi
fi 

#---------------- STOP COMMAND ----------------#
if [ $1 == ${commands[1]} ]; then
# start only 1 process
	if [ $2 != "all" ]; then
		stop_process $2
	else
	# start a process for each instrument
		for i in "${software_components[@]}"; do
				stop_process $i
		done 
		for i in "${instruments[@]}"; do
				stop_process $i
		done
	fi
fi

#---------------- RESTART COMMAND ----------------#
if [ $1 == ${commands[2]} ]; then
	# start only 1 process
	if [ $2 != "all" ]; then
		stop_process $2
		echo "sleeping 10s..."
		sleep 10
		start_process $2
	else
		for i in "${software_components[@]}"; do # stop all processes
				stop_process $i
		done 
		for i in "${instruments[@]}"; do # stop all processes
				stop_process $i
		done 

		echo "sleeping 10s..."
		sleep 10 #sleep 10s to avoid TCP errors


		for i in "${software_components[@]}"; do # start all processes
				start_process $i
		done
		for i in "${instruments[@]}"; do # stop all processes
				start_process $i
		done
	fi
fi

#---------------- STATUS COMMAND ----------------#
if [ $1 == ${commands[3]} ]; then
	if [ $2 == "all" ]; then
		for i in "${instruments[@]}"; do
			check_status_info $i
		done
		for i in "${software_components[@]}"; do
			check_status_info $i
		done
	else 
	check_status_info $2		
		show_last_meas $2
	fi
fi

# table
printf "\n"


cat $PIDs_file



#!/bin/bash
#===================================================================#
#                      Auto connect script                          #
#===================================================================#
# This script periodcally checks the status of the acquisition      #
# system, and if a process has been stopped unexpectadly it is      #
# launched again using the acquisition_manager.sh script            #
#===================================================================#
# @author : Enoc Martínez                                           #
# @contact : enoc.martinez@upc.edu                                  #
# @organization : Universitat Politécnica de Catalunya	(UPC)       #
#                                                                   #
# This script has been developed as a part of the EMSODEV project   #
#===================================================================#

manager=/home/sarti/testbed/acquisition_manager.sh
PIDs_file=/home/sarti/testbed/.PIDs
sleep_time=60

instruments=("SBE37" "EGIM_status" "ECO_NTU" "Aanderaa_4381" "SBE54" "ADCP_workhorse")
log_path=/home/sarti/testbed/log


#---------- Colour codes --------------#
red=$'\e[1;31m'
grn=$'\e[1;32m'
yel=$'\e[1;33m'
blu=$'\e[1;34m'
mag=$'\e[1;35m'
cyn=$'\e[1;36m'
end=$'\e[0m'


while true; do
	#updating log name and path
	year=$(date +%Y)
	if [ ! -e $log_path/$year ]; then
		echo "creating new directory $log_path/$year... "
		mkdir $log_path/$year -m 775
	fi

	month=$(date +%m)
	if [ ! -e $log_path/$year/$month ]; then
		echo "creating new directory $log_path/$year/$month... "
		mkdir $log_path/$year/$month  -m 775
	fi

	log=$log_path/$year/$month/auto_connect_$(date '+%d-%m-%y').log 
	touch $log # generate the file

	printf "\n\n==============================================\n" >> $log
	printf "\n\n==============================================\n"
	date >> $log
	printf  "\n==============================================\n"
	printf  "\n==============================================\n">> $logs
	date
	$manager status all > /dev/null # update the PIDs file using the manager script
	cat $PIDs_file >> $log
	cat $PIDs_file
	restarts=0
	for i in "${instruments[@]}"; do # stop all processes
		status=$(cat $PIDs_file | grep $i | awk '{print $2}') # get the status of instrument
		echo "status $status"
		if [ $status != "running" ]; then # if the status is not running, i.e. stop
			printf "${red}restarting $i...${end} " >> $log
			date >> $log
			printf "${red}restarting acquisition...${end}"
			date >> $log
			
			$manager start $i >> /dev/null # start the instrument
			let restarts=1 
		fi 
	done
	if [ $restarts == 0 ];  then
		echo "${grn}everything is ok...${end}" >> $log
		echo "${grn}everything is ok...${end}"
	fi	
	sleep $sleep_time
done
exit 






















	SBE_37_status=$(cat $PIDs_file | grep SBE37 | awk '{print $2}') # get the status of the SBE37
	echo "SBE_37_status is $SBE_37_status" 

	if [ $SBE_37_status != "running" ]; then # if the status is not running, i.e. stop
		printf "${red}restarting acquisition...${end} " >> $log
		date >> $log
		printf "${red}restarting acquisition...${end}"
		# stop all tcp instruments
		for i in "${tcp_instruments[@]}"; do
			if [ $i != "all" ]; then
				$manager stop $i >> /dev/null
			fi
		done 

		echo "wait $stop_start_time s between stop and start in order to free TCP ports..."
		sleep $stop_start_time
				
		# start all tcp instruments
		for i in "${tcp_instruments[@]}"; do
			if [ $i != "all" ]; then
				$manager start $i >> /dev/null
			fi
		done
		echo "waiting $stop_start_time to wake up the acquisition agents"
		sleep $stop_start_time
		$manager status all > /dev/null # update the PIDs file using the manager script
		cat $PIDs_file >> $log
	elif [ $SBE_54_status != "running" ]; then # if the status is not running, i.e. stop 
		echo "${grn}restarting  is ok...${end}" >> $log

	else
		echo "${grn}everything is ok...${end}" >> $log
		echo "${grn}everything is ok...${end}"
	fi

	SBE_54_status=$(cat $PIDs_file | grep SBE54 | awk '{print $2}') # get the status of the SBE54
	if [ $SBE_54_status != "running" ]; then 
		printf "${red}restarting SBE54...${end} " >> $log
		date >> $log
		$manager start SBE54 >> /dev/null
	fi
	echo "sleeping $sleep_time seconds..." >> $log
	echo "sleeping $sleep_time seconds..."
	sleep $sleep_time
done

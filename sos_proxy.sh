#!/bin/bash
#===================================================================#
#                       SOS Proxy script                            #
#===================================================================#
# This script searches for files in "source_path" and sends it to	#
# the SOS server. Afterwards the sent files are moved to the		#
# "destination_path", organizing them in folders by year, month		#
# and day:															#
#																	#
# dest_path/year_month/day/file.xml									#
#===================================================================#
# @author : Enoc Martínez											#
# @contact : enoc.martinez@upc.edu									#
# @organization : Universitat Politécnica de Catalunya	(UPC)		#
#																	#
# This script has been developed as a part of the EMSODEV project	#
#===================================================================#


#IP=https://147.83.159.168:9443/52n-sos-webapp
IP=https://192.168.1.132:9443/52n-sos-webapp
DMP_IP=http://dmpnode6.emsodev.eu:29999/contentListener

enable_dmp=TRUE
#IP=http://147.83.159.158:8080/52n-sos-webapp

source_path=/home/sarti/testbed/files
destination_path=/home/sarti/testbed/sent_files
log_path=/home/sarti/testbed/log


# if the destination path does not exist
if [ ! -e $source_path ]; then
	echo "source path $source_path does not exist, exit"
	exit 0
fi

# if the destination path does not exist
if [ ! -e $destination_path ]; then
	echo "destination path $destination_path does not exist, exit"
	exit 0
fi

# if the destination path does not exist
if [ ! -e $log_path ]; then
	echo "log path $log_path does not exist, exit"
	exit 0
fi


## Start script
echo "starting script"
echo "files will be sent to $IP"

cd /home/sarti/testbed

# if the destination path does not exist
if [ ! -e $destination_path ]; then
	echo "destination path $destination_path does not exist, exit"
	exit 0
fi

while : ; do
	#name log file
#updating log name and path
	year=$(date +%Y)
	if [ ! -e $log_path/$year ]; then
		echo "creating new directory $log_path/$year... "
		mkdir $log_path/$year -m 775
	fi

	month=$(date +%m)
	if [ ! -e $log_path/$year/$month ]; then
		echo "creating new directory $log_path/$year/$month... "
		mkdir $log_path/$year/$month  -m 775
	fi

	log=$log_path/$year/$month/sos_proxy_$(date '+%d-%m-%y').log 
	touch $log # generate the file

	for file in $source_path/*.xml ; do
		if [ -e $file ]; then
			if [ -e $1 ]; then

				# Send the file to the SOS server
				printf "\n--------------------------------------------------------------------\n" >> $log
				date >> $log
				printf "Sending file $file \n" >> $log

				printf "\n--------------------------------------------------------------------\n"
				date
				printf "Sending file $file \n"

				#send the file to the SOS server
				wget --no-check-certificate --output-file=http_response \
						--output-document=sos_response \
						--header="Content-type: application/xml boundary=FILEUPLOAD" \
						--post-file $file $IP/service/pox 

				if [ -e http_response ]; then
					cat http_response | grep HTTP
					cat http_response | grep HTTP >> $log
					rm http_response
				fi

				if [ -e sos_response ]; then	
					cat sos_response | grep Exception
					cat sos_response | grep Exception >> $log
					rm sos_response
				fi

				if [ enable_dmp == TRUE ]; then
					wget --no-check-certificate --output-file=dmp_response \
							--output-document=dmp_response \
							--header="Content-type: application/xml" \
							--post-file $file $DMP_IP 

					if [ -e dmp_http ]; then
						rm dmp_http
					fi

					if [ -e dmp_response ]; then	
						rm dmp_response
					fi
				fi

	
				printf "\n" >> $log


				# Move the file to dest_path/year/month/day/file.xml

				year=$(date +%Y)
				if [ ! -e $destination_path/$year ]; then
					echo "creating new directory $destination_path/$year... "
					mkdir $destination_path/$year
				fi

				month=$(date +%m)
				if [ ! -e $destination_path/$year/$month ]; then
					echo "creating new directory $destination_path/$year/$month... "
					mkdir $destination_path/$year/$month
				fi

				day=$(date +%d)
				if [ ! -e $destination_path/$year/$month/$day ]; then
					echo "creating new directory $destination_path/$year/$month/$day..."
					mkdir $destination_path/$year/$month/$day
				fi
		

				echo "moving file $file to $destination_path/$year/$month/$day"
				mv $file $destination_path/$year/$month/$day
				sleep 1

			else 
				echo "waiting for files...."
			fi
		fi
	done
	sleep 10
done
exit


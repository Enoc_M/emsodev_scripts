#!/bin/bash
#===================================================================#
#                        Hydrophone FTP script                      #
#===================================================================#
# This script periodically gets queries the icListen hydrophone     #
# in order to get a list of all the files stored in its memory.     #
# Afterwards, every .wav and .txt file are downloaded via ftp and   #
# stored in the EMSODEV NAS.									    #
#                                                                   #
#===================================================================#
# @author : Enoc Martínez                                           #
# @contact : enoc.martinez@upc.edu                                  #
# @organization : Universitat Politécnica de Catalunya	(UPC)       #
#                                                                   #
# This script has been developed as a part of the EMSODEV project   #
#===================================================================#

#---------- Variables --------------#

# Hydrophone IP
icListen_IP="192.168.1.212"
# path where the insertResult files will be generated
ir_path="/home/sarti/hydrophone/insertResults"       
# path where the files will be stored
download_path="/home/sarti/hydrophone/downloads"     
# path where the files will be stored
wav_template=icListen_SB60L-ETH-1636_waveform
# template that will be used for the spectrogram files
spectrogram_template=icListen_SB60L-ETH-1636_spectrum 

#---------- Colour codes --------------#
red=$'\e[1;31m'
grn=$'\e[1;32m'
yel=$'\e[1;33m'
blu=$'\e[1;34m'
mag=$'\e[1;35m'
cyn=$'\e[1;36m'
end=$'\e[0m'

#---------------------------------------------------------------------------#
#--------------------------- FUNCTION DEFINITIONS --------------------------#
#---------------------------------------------------------------------------#

#-------------------------------------------------------------------#
#-------------------------------- msg ------------------------------#
#-------------------------------------------------------------------#
# This function echoes its argument and, at the same time, stores   #
# them to the log file. the log filename has to be defined          #
#-------------------------------------------------------------------#
msg(){
	printf "$1\n"
	printf "$1\n" >> $log
}

#-------------------------------------------------------------------#
#---------------------------- getFileList --------------------------#
#-------------------------------------------------------------------#
# This function connects to the hydrophone IP via ftp and lists its #
# contens. Afterwards their contents are separated into .wav and    #
# .txt files (spectrograms). The lists are stored in the            #
# "wav_files_list" and "spectrogram_files_list"                     #
#-------------------------------------------------------------------#
getFileList(){
#connecto via FTP and list all files, redirect output to ftp_output.tmp
	msg "getting file list..."
	#store a list in file ftp_output.tmp
	ftp -n $icListen_IP > ftp_output.tmp  <<ENDFTP
	cd Data
	ls
	bye
ENDFTP
	cat ftp_output.tmp | while read line; do # read all lines, one by one
		    n=$[$n+1]
		    filename=$(echo "$line" | awk '{print $9}' )   # extract the filename from the 'ls' list
		    if [[ $filename = *[!\ ]* ]]; then             # if the filename is not empty
				if [ ${line:0:1} != d ]; then              # if this entrance is NOT a directory
					if [ ${filename: -4} == ".wav" ]; then # store this line into wav files if its .wav
						echo "$filename" >> wav_files_list
					elif [ ${filename: -4} == ".txt" ]; then # check the extension
						echo "$filename" >> spectrogram_files_list
					fi
				fi	
		    fi
	done

	rm ftp_output.tmp
	msg "OK"
}

#-------------------------------------------------------------------#
#------------------------------- getFile ---------------------------#
#-------------------------------------------------------------------#
# This function connects to the hydrophone IP via ftp, and gets the #
# file specified in arg 1. Afterwards the file is deleted.          #
#-------------------------------------------------------------------#
getfile() {
# gets a file passed as argument and then deletes it
	msg "getting file $1"
	ftp -n $icListen_IP > get_file_ftp.tmp <<ENDFTP
	bin
	cd Data
	lcd $download_path
	get $1
	delete $1
	bye
ENDFTP
	error=$(cat get_file_ftp.tmp | grep Error)
	if [[ $error = *[!\ ]* ]]; then  
		msg "${red}Error $error getting file $1${end}"
		return 0
	else
		msg "${grn}ftp success, generating insert result for file $1${end}"
		generateInsertResult $1
	fi
	rm get_file_ftp.tmp
	msg "exit ftp"
}

#-------------------------------------------------------------------#
#----------------------- generateInsertResult ----------------------#
#-------------------------------------------------------------------#
# This function takes a filename as input and generates a O&M       #
# insertResult file, whose data is a link to a ftp download from the#
# server. The timestamp is generated according to the filename and  #
# the template changes depending if it is wav file or a spectrogram #
#-------------------------------------------------------------------#

#-------- Insert Result constants --------#
header='<?xml version="1.0" encoding="UTF-8"?>
<sos:InsertResult xmlns:sos="http://www.opengis.net/sos/2.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" service="SOS" version="2.0.0"
    xsi:schemaLocation="http://www.opengis.net/sos/2.0 http://schemas.opengis.net/sos/2.0/sos.xsd">
    <sos:template>'

after_template='</sos:template>
	<sos:resultValues>'

ending='</sos:resultValues>
</sos:InsertResult>'
#------------------------------------------#

generateInsertResult() {
	#generate the timestamp according to the name of the file (2017-12-31T13:59:00+00:00)
	timestamp="${1:8:4}-${1:12:2}-${1:14:2}T${1:17:2}:${1:19:2}:${1:21:2}+0:00"

#use wav_template or spectrogram_template depending on the extension of the file
	if [ ${1: -4} == ".wav" ]; then
		echo "WAV - $1"
		template=$wav_template
		filename="$ir_path/hyd_wav_${1:8:4}-${1:12:2}-${1:14:2}_${1:17:2}:${1:19:2}:${1:21:2}.xml"
	else
		echo "TXT - $1"
		template=$spectrogram_template
		filename="$ir_path/hyd_spec_${1:8:4}-${1:12:2}-${1:14:2}_${1:17:2}:${1:19:2}:${1:21:2}.xml"
	fi
	data="ftp:/$path/$filename"
	echo "filaneme is $filename"
	# Generate the insertResult file 
	printf "$header" > $filename
	printf "$template" >> $filename
	printf "$after_template" >> $filename
	printf "$template#$data@" >> $filename
	printf "$ending" >> $filename
}


#---------------------------------------------------------------------------#
#------------------------------ PROGRAM START ------------------------------#
#---------------------------------------------------------------------------#
log="script_hydrophone.log"

#remove previous files
msg "starting hydrophone script"



while : ; do #infinite loop

	#delete old list files
	if [ -e wav_files_list ]; then
		    rm wav_files_list
	fi

	if [ -e spectrogram_files_list ]; then
		    rm spectrogram_files_list
	fi

	getFileList #get the lists of wav and spectrograms
	if [ -e spectrogram_files_list ];then 
		cat spectrogram_files_list | while read spectfile; do # for each file in the list
			getfile $spectfile		
		done
	fi	

	if [ -e wav_files_list ]; then	
		cat wav_files_list | while read wavfile; do # for each file in the list
			msg "getting file $wavfile"
			getfile $wavfile		
		done
	fi
	msg "sleeping..."
	sleep 10
done
exit

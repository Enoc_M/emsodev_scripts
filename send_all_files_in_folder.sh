#!/bin/bash

#IP=https://147.83.159.168:9443/52n-sos-webapp
IP=https://192.168.1.132:9443/52n-sos-webapp
red=$'\e[1;31m'
grn=$'\e[1;32m'
yel=$'\e[1;33m'
blu=$'\e[1;34m'
mag=$'\e[1;35m'
cyn=$'\e[1;36m'
end=$'\e[0m'

## Start script
echo "starting script"
echo "files will be sent to $IP"

array=("01" "02" "03" "04" "05" "06" "07")

for i in "${array[@]}"; do # Check if the command argument is correct
	for file in $i/*.xml ; do
		sleep 1
		if [ -e $file ]; then
			if [ -e $1 ]; then

				# Send the file to the SOS server
				printf "${blu}\n--------------------------------------------------------------------\n"
				date
				printf "sending file $file \n${end}"

				#send the file to the IP
				wget --no-check-certificate --output-file=http_response \
						--output-document=sos_response \
						--header="Content-type: application/xml boundary=FILEUPLOAD" \
						--post-file $file $IP/service/pox 

				if [ -e http_response ]; then
					cat http_response
					rm http_response
				fi

				if [ -e sos_response ]; then	
					cat sos_response 
					rm sos_response
				fi
			fi
		fi
	done
done

	

#!/bin/bash

#script to download all the emsodev scripts from the emsodev_scripts repo at bitbucket.org


#define all the urls to download
acquisition_manager_url=https://bitbucket.org/Enoc_M/emsodev_scripts/raw/b3f80cc7655c3e9fc4ee61ed7b3d867f39bc90c7/acquisition_manager.sh
auto_connect_url=https://bitbucket.org/Enoc_M/emsodev_scripts/raw/b3f80cc7655c3e9fc4ee61ed7b3d867f39bc90c7/auto_connect.sh
sos_proxy_url=https://bitbucket.org/Enoc_M/emsodev_scripts/raw/b3f80cc7655c3e9fc4ee61ed7b3d867f39bc90c7/sos_proxy.sh
download_scripts_url=https://bitbucket.org/Enoc_M/emsodev_scripts/raw/67fc817c196cb6fa470de9e17b33e6206256f719/download_scripts.sh

#define the paths
scripts_path=/home/sarti/testbed
old_scripts_path=/home/sarti/testbed/.old_scripts

#define the script names
acquisition_manager=$scripts_path/acquisition_manager.sh
auto_connect=$scripts_path/auto_connect.sh
sos_proxy=$scripts_path/sos_proxy.sh
download_scripts=$scripts_path/download_scripts.sh

#declare an array for the urls and other for the names
scripts_urls=("$acquisition_manager_url" "$auto_connect_url" "$sos_proxy_url" "$download_scripts_url")
scripts_names=("$acquisition_manager" "$auto_connect" "$sos_proxy" "$download_scripts" )

#----------------------------------------------------------------#
#---------------------- Script starts here ----------------------#
#----------------------------------------------------------------#

#If old scripts folder does not exist, create if
if [ ! -e $old_scripts_path ]; then
	echo "creating new directory ... "
	mkdir $old_scripts_path
fi

index=0
#loop through scripts
for i in "${scripts_names[@]}"; do
	echo "first name $i"
	url=${scripts_urls[$index]}
	
#if they already exist, move them to the old scripts folder
	if [ ! -e ${scripts_names[index]} ]; then
		echo "copying old script to old_scripts_path"
		mv $i  $old_scripts_path
	fi

	wget -O $i $url #get the new script from the web
	chmod +x $i #give execution permissions to the script
	((index++)) #increment the counter
	echo "new index $index"
done
